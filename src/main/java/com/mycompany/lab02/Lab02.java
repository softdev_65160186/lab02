/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab02;

import java.util.Scanner;

/**
 *
 * @author HP
 */
public class Lab02 {
    static char[][] table = {{'-','-','-'},
                            {'-','-','-'},
                            {'-','-','-'}
    };
    static char currentPlayer = 'X';
    static int row, col;
    static String answer;
    
    static void printWelcome(){
        System.out.println("Welcome OX");
    }
    static void printtable(){
        for (int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table [i][j] + " ");
            }
            System.out.println("");
        }
    }
    static void printTurn(){
        System.out.println(currentPlayer + " Turn");
    }
    static void inputRowCol(){
        Scanner sc = new Scanner(System.in);
        while(true){
        System.out.print("Please input row col :");
        row = sc.nextInt();
        col = sc.nextInt();
        if (table[row-1][col-1] == '-') {
          table [row-1][col-1] = currentPlayer;
          break;
            }
        }
    }
    
    static void switchOX() {
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                if(table[i][j]=='X') {
                    table[i][j] = '-';
                }else{
                    table[i][j] = '-' ;
                }
            }
        }
    }
    
    static void  switchPlayer(){
        if(currentPlayer=='X') {
            currentPlayer ='O';
        } else {
            currentPlayer = 'X';
        }
    }
    static void isContinue() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Continue? (y/n): ");
            answer = kb.next();
            if (answer.equals("n")) {
                System.out.println("Thanks for playing!");
                System.exit(0);
            } else if (answer.equals("y")) {
                break;
            } else {
                System.out.println("Invalid input. Please try again.");
            }
        }
    }
    ////  check /////
    static boolean checkRow(){
        for (int i=0;i<3;i++){
            if(table[row-1][i]!=currentPlayer){
                return false;
            }
        }
        return true; 
    }
    static boolean checkCol() {
        for(int i=0;i<3;i++) {
            if(table[i][col-1]!=currentPlayer) {
                return false;
            }
        }
        return true;
    }
    
    static boolean checkDiagonal1() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }
    static boolean checkDiagonal2() {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }
    
    static boolean isWin(){
        if(checkRow()||checkCol()||checkDiagonal1()||checkDiagonal2()){
            return true;
        }
        return false;
    }
    
    static boolean stop(){
        return false;
    }
    
    static boolean isDraw(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(table[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    }
    //// print result ////
    static void printWin(){
        System.out.println(currentPlayer + " Win!!");
    }
    
    static void printDraw() {
        System.out.println("It's a draw!");
    }
    ///// main ////
    public static void main(String[] args) {
        printWelcome();
        while(true){
            while(true) {
                printtable();
                printTurn();
                inputRowCol();
                if (isWin()) {
                    printtable();
                    printWin();
                    isContinue();
                    break;
                }else if (isDraw()) {
                    printtable();
                    printDraw();
                    isContinue();
                    break;
               }
                switchPlayer();
            }
            switchOX();
        }
    }
}
